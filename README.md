## What is it

Test HTTP server to build in a Docker container and deploy to [Puzl cloud](https://puzl.ee)). Server simply runs Swagger API on `1616` port.

## Requirements

- Account on [Docker Hub](https://hub.docker.com)
- Account on [Puzl cloud](https://puzl.ee)

### Swagger port

Add port `1616` via Puzl dashboard. 

![Open port in Puzl dashboard](port-screenshot.png?raw=true "Open port")

Kubernetes `Service` will be created for your pod automatically.

### Environment variables

You can define your own port, if needed.

`LISTEN_PORT` - Swagger API, default `1616`

## Use

After your pod is up and running in Puzl Kubernetes cluster, you can access it by a given external port and host name.

`http://external_host:external_port/swagger`

### Routes

`/health` - used to check health

`/swagger` - Swagger endpoint
